import axios from 'axios'

const state = () => ({
  token: '',
  user: {},
  status: false
})

const getters = {
  isAuth(state) {
    return state.status
  },
  getUser(state) {
    return state.user
  },
  isAdmin(state) {
    return state.isAdmin
  }
}

const mutations = {
  setUser(state, user) {
    state.user = user
  },
  setAuth(state, {response}) {
    state.token = response.token
    state.user = response.user
    state.status = response.success
  },
  setToken(state, token) {
    state.status = true
    state.token = token
  },
  notAuth(state) {
    state.status = false
  },
  logout(state) {
    state.token = ''
    state.user = ''
    state.status = false
  },
  setCurrentEmail(state, email) {
    if (process.client) {
      localStorage.setItem('currentEmail', email)
    }
  }
}

const actions = {
  async login({commit}, user) {
    try {
      const res = await axios.post('/api/users/login', user)
      if (res.status == 200) {
        const response = res.data
        if (process.client) {
          localStorage.setItem('token', response.token)
        }
        axios.defaults.headers.common['Authorization'] = response.token
        commit('setAuth', {response})
      }
      return res
    } catch (err) {
      return err.response
    }
  },

  async registration({commit}, data) {
    try {
      const res = await axios.post('/api/users/register', data)
      return res
    } catch (err) {
      return err.response
    }
  },

  async getUser({commit}) {
    try {
      const res = await axios.get('/api/users/profile')
      const user = await res.data.user
      commit('setUser', user)
      return user
    } catch (err) {
      this.dispatch('auth/logout')
      return err.response
    }
  },

  logout({commit}) {
    if (process.client) localStorage.removeItem('token')
    delete axios.defaults.headers.common['Authorization']
    commit('logout')
    this.commit('admin/setAdmin', false)
    this.$router.push('/login')
    return
  },

  checkAuth({commit}) {
    if (process.client) {
      const token = localStorage.getItem('token') || undefined
      if (token) {
        commit('setToken', token)
        axios.defaults.headers.common['Authorization'] = token
        this.dispatch('admin/checkIfAdmin')
      }
      else {
        delete axios.defaults.headers.common['Authorization']
        commit('notAuth')
      }
    }
  },

  async verifyEmail({commit}, email) {
    const data = {email: email}
    try {
      const res = await axios.post('/api/users/verify', data)
      return res
    } catch (err) {
      return err.response
    }
  },

  async verified({commit}, email) {
    const data = {email: email}
    try {
      const res = await axios.post('/api/users/verified', data)
      return res
    } catch (err) {
      return err.response
    }
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
