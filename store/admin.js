import axios from 'axios'

const state = () => ({
  isAdmin: false,
  users: {}
})

const getters = {
  isAdmin(state) {
    return state.isAdmin
  },
  usersList(state) {
    return state.users
  }
}

const mutations = {
  setAdmin(state, value) {
    state.isAdmin = value
  },
  setUsers(state, users) {
    state.users = users
  }
}

const actions = {
  async getAllUsers({commit}) {
    try {
      const res = await axios.get('/api/users/list')
      const list = await res.data.users
      commit('setUsers', list)
      return state.users
    } catch (err) {
      return err.response
    }
  },
  async checkIfAdmin({commit}, email) {
    try {
      const res = await axios.get('/api/users/profile')
      if (res.data.user.email === '18art12@gmail.com') {
        commit('setAdmin', true)
      } else {
        commit('setAdmin', false)
      }
      return res
    } catch (err) {
      commit('setAdmin', false)
      return err.response
    }
  },
  async deleteUser({commit}, data) {
    const res = await axios.post('/api/users/deleteuser', data)
    return res
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
