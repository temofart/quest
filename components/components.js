import Vue from 'vue'

Vue.component('loader', () => import('../components/loader'))
Vue.component('Slider', () => import('../components/index/Slider'))
Vue.component('About', () => import('../components/index/About'))
Vue.component('quests', () => import('../components/index/quests'))
Vue.component('Subscribe', () => import('../components/index/Subscribe'))
Vue.component('faq', () => import('../components/index/FAQ'))

Vue.component('Header', () => import('../components/Header'))
Vue.component('Footer', () => import('../components/Footer'))
Vue.component('ClientSidebar', () => import('../components/ClientSidebar'))
