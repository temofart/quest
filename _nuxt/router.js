import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _51de4252 = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _51815b97 = () => interopDefault(import('../pages/profile.vue' /* webpackChunkName: "pages/profile" */))
const _da1f2e12 = () => interopDefault(import('../pages/quest-1/index.vue' /* webpackChunkName: "pages/quest-1/index" */))
const _638070bb = () => interopDefault(import('../pages/registration.vue' /* webpackChunkName: "pages/registration" */))
const _0d693bc0 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/login",
    component: _51de4252,
    name: "login"
  }, {
    path: "/profile",
    component: _51815b97,
    name: "profile"
  }, {
    path: "/quest-1",
    component: _da1f2e12,
    name: "quest-1"
  }, {
    path: "/registration",
    component: _638070bb,
    name: "registration"
  }, {
    path: "/",
    component: _0d693bc0,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
