const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const passport = require('passport')
const User = require('../model/User')
const secretKey = require('../config/passport-key').secretKey
const adminEmail = require('../config/admin').email
const sendMail = require('./mailing')

router.post('/register', (req, res) => {
  let {username, email, password} = req.body

  User.findOne({email: email}).then(client => {
    if (client) {
      return res.status(400).json({
        success: false,
        status: 400
      })
    } else {
      let newUser = new User({
        username,
        email,
        password
      })

      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser.save().then(user => {
            return res.status(201).json({
              success: true,
              user: user
            })
          })
        })
      })
    }
  })
})

router.post('/login', (req, res) => {
  User.findOne({email: req.body.email}).then(user => {
    if (!user) {
      return res.status(404).end()
    } else {
      bcrypt.compare(req.body.password, user.password).then(isMatch => {
        if (isMatch) {
          const payload = {
            id: user._id,
            username: user.username,
            email: user.email
          }
          jwt.sign(payload, secretKey, {expiresIn: "31d"}, (err, token) => {
            return res.status(200).json({
              success: true,
              token: `Bearer ${token}`,
              user: user,
            })
          })
        } else {
          return res.status(401).end()
        }
      })
    }
  })
})

router.post('/verify', (req, res) => {
  const email = req.body.email
  let code = null
  const generator = () => code = (Math.random() * (9999 - 1000) + 1000).toFixed(0)
  generator()
  console.log(code)

  sendMail(email, code)
  User.updateOne({email: email}, {$set: {"pin": code}})
    .then(() => {
      return res.status(200).json({pin: code})
    })
})

router.post('/verified', (req, res) => {
  const email = req.body.email

  User.updateOne(
    {email: email},
    {$set: {'verified': true}, $unset: {'pin': ""}}
  )
    .then(() => {
      return res.status(200).end()
    })
})

router.get('/profile', passport.authenticate('jwt', {session: false}), (req, res) => {
  console.log(req.user)
  return res.status(200).json({
    user: req.user
  })
})

router.get('/list', passport.authenticate('jwt', {session: false}), (req, res) => {
  User.find().then(users => {
    return res.status(200).json({users: users})
  })
})

router.post('/deleteuser', (req, res) => {
  const id = req.body.id
  const email = req.body.email
  if (email === adminEmail) return res.status(200).json({ok: false})

  User.deleteOne({_id: id})
    .then(() => {
      return res.status(200).json({ok: true})
    })
    .catch(err => {
      return res.status(200).json({ok: false})
    })
})

module.exports = router
