const mongoose = require('mongoose')
const Schema = mongoose.Schema
const date = new Date()

const UserSchema = new Schema({
  username: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  created: {
    type: String,
    default: date.toUTCString()
  },
  lastVisited: {
    type: String,
    default: date.toUTCString()
  },
  subscribe: {
    type: String
  },
  pin: {
    type: String
  },
  verified: {
    type: Boolean,
    default: false
  }
})

module.exports = User = mongoose.model('users', UserSchema)
