const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const cors = require('cors')
const { Nuxt, Builder } = require('nuxt')
const passport = require('passport')

const app = express()
app.use(bodyParser.urlencoded({
  extended: false
}))

app.use(bodyParser.json())
app.use(cors())
app.use(passport.initialize())

// Set up DB
const { db } = require('./config/database')
mongoose.connect(db, { useNewUrlParser: true })
  .then(() => console.log(`[OK] MongoDB is connected: ${db}`))
  .catch(err => console.error(err));
mongoose.Promise = global.Promise

// Import and Set Nuxt.js options
const config = require('../nuxt.config.js')
config.dev = process.env.NODE_ENV !== 'production'

async function start () {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  } else {
    await nuxt.ready()
  }

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen({port, host}, () => {
    console.log(`Server started on ${host}:${port}`)
  })
}
start()

const users = require('./api/users')
app.use('/api/users', users)

require('./config/passport')(passport)
