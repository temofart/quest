module.exports = {
  mode: 'universal',

  head: {
    title: "WebQuest Club",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'keywords', content: 'квест, онлайн, веб, игра, интеллектуальная, вебквест, онлайн квест, онлайн игры, развивающие игры, интеллектуальные игры, умные игры, квесты онлайн, webquest, WebQuest, web quest, детективы, интерактивная игра, игры для айтишников, задачи и головоломки, квесты в интернете'},
      { name: 'description', content: 'Web Quest - Первый онлайн квест с неограниченными возможностями. Новый формат онлайн игр, где предстоит выйти за рамки типичного понимания игры. Невероятно интересный, интеллектуальный, развивающий проект. Веб квест содержит множество интересных задач повышенной сложности и рассчитан на тех, кому интересен детективный жанр, различные головоломки и процесс решения сложных задач. И все это - онлайн. Если вы в поиске такого проекта - приглашаем вас к нам на webquest.club' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }
    ]
  },
  css: [
    '@/assets/styles/main.scss'
  ],
  plugins: [
    '@/components/components.js',
    '@/components/index.js'
  ],
  modules: [
    '@nuxtjs/axios',
  ],
  router: {
    scrollBehavior: function (to, from, savedPosition) {
      return { x: 0, y: 0 }
    }
  },
  build: {
    publicPath: process.env.NODE_ENV == 'production' ? 'dist' : false
  },
  buildModules: [
    ['@nuxtjs/google-analytics', {
      id: 'UA-157930178-1',
      dev: false
    }]
  ]
}
